import sys
import maya.cmds as mc
import maya.api.OpenMaya as OpenMaya


curve = 'curve1'

steps = 50

activeSelectionList = OpenMaya.MGlobal.getActiveSelectionList()

selection = activeSelectionList.getComponent(0)

if mc.objExists('topLocatorGroup'):
    mc.delete('topLocatorGroup')

topLocatorGroup = mc.group(name='topLocatorGroup', empty=True, world=True)

if selection[0].hasFn(OpenMaya.MFn.kNurbsCurve):
    curve = OpenMaya.MFnNurbsCurve(selection[0])

    maxRange = curve.knotDomain[1]

    counter = 1
    for param in xrange(0, steps + 1):
        normalizedParam = float(param) / steps * maxRange

        normalVector = curve.normal(normalizedParam)
        normalVector.normalize()

        tangentVector = curve.tangent(normalizedParam)
        tangentVector.normalize()

        outsideVector = normalVector ^ tangentVector

        print normalVector, tangentVector

        pointOnCurve = curve.getPointAtParam(normalizedParam)

        normalPoint = pointOnCurve + normalVector
        # normalLocator = mc.spaceLocator(position=[normalPoint.x, normalPoint.y, normalPoint.z], name='x1_{}'.format(counter))
        # mc.parent(normalLocator, topLocatorGroup)

        normalPoint = pointOnCurve - normalVector
        # normalLocator = mc.spaceLocator(position=[normalPoint.x, normalPoint.y, normalPoint.z], name='x2_{}'.format(counter))
        # mc.parent(normalLocator, topLocatorGroup)

        tangentPoint = pointOnCurve + tangentVector
        # tangentLocator = mc.spaceLocator(position=[tangentPoint.x, tangentPoint.y, tangentPoint.z], name='tangentVector_{}'.format(counter))
        # mc.parent(tangentLocator, topLocatorGroup)

        outsidePoint = pointOnCurve + outsideVector
        outsideLocator = mc.spaceLocator(position=[outsidePoint.x, outsidePoint.y, outsidePoint.z], name='outsideVector_{}'.format(counter))
        mc.parent(outsideLocator, topLocatorGroup)

        # pointLocator = mc.spaceLocator(position=[pointOnCurve.x, pointOnCurve.y, pointOnCurve.z], name='pointOnCurve_{}'.format(counter))
        # mc.parent(pointLocator, topLocatorGroup)

        counter += 1


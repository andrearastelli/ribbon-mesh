import sys

import maya.api.OpenMaya as OpenMaya


kRibbonNodeName = 'ribbonNode'


##
## @brief      Function used to tell Maya that the new Python API 2.0 needs to
##             be used to manage this plugin
##
## @return     None
##
def maya_useNewAPI():
    pass


##
## @brief      Ribbon Node class.
##
## @details    This class extends the base Node object of the OpenMaya API. This
##             node gets as input a NURBS curve and uses this curve to generate
##             a ribbon mesh (flat). For the profile of the Ribbon a Ramp
##             attribute is used.
##
##             This Ramp will be used, along the profile of the Ribbon, to
##             define how much, from the NURBS curve, the profile of the ribbon
##             needs to be distanced. A value from 0 to 1 is accepted. All other
##             value will be clumped to 1 or 0.
##
##             The output of the node is in the outRibbonMesh plug and is a
##             proper mesh that can me attached to a polygonMesh node to be
##             displayed in the viewport.
##
class RibbonNode(OpenMaya.MPxNode):

    ##
    ## ID used to uniquely identify this Node in Maya
    ##
    id = OpenMaya.MTypeId(0x00FF1111)

    ##
    ## Input nurbs curve, used as base structure for the Ribbon
    ##
    inNurbsCurve = None

    ##
    ## Defines the width of the Ribbon
    ##
    inRibbonWidth = None

    ##
    ## Number of vertical subdivision along the NURBS used to generate the final
    ## mesh
    ##
    inRibbonCurveSubdivisions = None

    ##
    ## Number of base subdivisions used as base for the Ribbon mesh
    ##
    inRibbonBaseSubdivisions = None

    ##
    ## Ribbon profile curve multiplier to define the Ribbon shape
    ##
    inRibbonProfileCurve = None

    ##
    ## Output Ribbon Mesh
    ##
    outRibbonMesh = None

    ##
    ## @brief      Constructs the object.
    ##
    ## @param      self  The object
    ##
    def __init__(self):
        super(RibbonNode, self).__init__()

    ##
    ## @brief      Returns an instance of the Node
    ##
    ## @return     [RibbonNode] - Instance of the node
    ##
    @staticmethod
    def creator():
        return RibbonNode()

    ##
    ## @brief      Initialize all the input and output attributes of the node,
    ##             and tells the node what attribute drives the other
    ##
    ## @return     [None] - None
    ##
    @staticmethod
    def initialize():
        inNurbsCurveFn = OpenMaya.MFnTypedAttribute()
        RibbonNode.inNurbsCurve = inNurbsCurveFn.create('inNurbsCurve', 'inNC', OpenMaya.MFnData.kNurbsCurve)
        inNurbsCurveFn.storable = True
        inNurbsCurveFn.keyable = True
        inNurbsCurveFn.readable = True
        inNurbsCurveFn.writable = True
        OpenMaya.MPxNode.addAttribute(RibbonNode.inNurbsCurve)

        inRibbonWidthFn = OpenMaya.MFnNumericAttribute()
        RibbonNode.inRibbonWidth = inRibbonWidthFn.create('width', 'w', OpenMaya.MFnNumericData.kDouble, 1)
        inRibbonWidthFn.storable = True
        inRibbonWidthFn.keyable = True
        inRibbonWidthFn.readable = True
        inRibbonWidthFn.writable = True
        OpenMaya.MPxNode.addAttribute(RibbonNode.inRibbonWidth)

        inRibbonCurveSubdivisionsFn = OpenMaya.MFnNumericAttribute()
        RibbonNode.inRibbonCurveSubdivisions = inRibbonCurveSubdivisionsFn.create('curveSubdivisions', 'surveSubd', OpenMaya.MFnNumericData.kInt, 3)
        inRibbonCurveSubdivisionsFn.storable = True
        inRibbonCurveSubdivisionsFn.keyable = True
        inRibbonCurveSubdivisionsFn.readable = True
        inRibbonCurveSubdivisionsFn.writable = True
        inRibbonCurveSubdivisionsFn.setMin(3)
        OpenMaya.MPxNode.addAttribute(RibbonNode.inRibbonCurveSubdivisions)

        inRibbonBaseSubdivisionsFn = OpenMaya.MFnNumericAttribute()
        RibbonNode.inRibbonBaseSubdivisions = inRibbonBaseSubdivisionsFn.create('baseSubdivisions', 'baseSubd', OpenMaya.MFnNumericData.kInt, 1)
        inRibbonBaseSubdivisionsFn.storable = True
        inRibbonBaseSubdivisionsFn.keyable = True
        inRibbonBaseSubdivisionsFn.readable = True
        inRibbonBaseSubdivisionsFn.writable = True
        OpenMaya.MPxNode.addAttribute(RibbonNode.inRibbonBaseSubdivisions)

        inRibbonProfileCurveFn = OpenMaya.MRampAttribute()
        RibbonNode.inRibbonProfileCurve = inRibbonProfileCurveFn.createCurveRamp('profileCurve', 'profile')
        OpenMaya.MPxNode.addAttribute(RibbonNode.inRibbonProfileCurve)

        outRibbonMeshFn = OpenMaya.MFnTypedAttribute()
        RibbonNode.outRibbonMesh = outRibbonMeshFn.create('outMesh', 'mesh', OpenMaya.MFnData.kMesh)
        outRibbonMeshFn.storable = False
        outRibbonMeshFn.keyable = False
        outRibbonMeshFn.readable = True
        outRibbonMeshFn.writable = False
        OpenMaya.MPxNode.addAttribute(RibbonNode.outRibbonMesh)

        OpenMaya.MPxNode.attributeAffects(RibbonNode.inNurbsCurve, RibbonNode.outRibbonMesh)
        OpenMaya.MPxNode.attributeAffects(RibbonNode.inRibbonWidth, RibbonNode.outRibbonMesh)
        OpenMaya.MPxNode.attributeAffects(RibbonNode.inRibbonCurveSubdivisions, RibbonNode.outRibbonMesh)
        OpenMaya.MPxNode.attributeAffects(RibbonNode.inRibbonBaseSubdivisions, RibbonNode.outRibbonMesh)
        OpenMaya.MPxNode.attributeAffects(RibbonNode.inRibbonProfileCurve, RibbonNode.outRibbonMesh)

    ##
    ## @brief      Gets the rotation matrix from vector.
    ##
    ## @param      self    The object
    ## @param      [MVector] vector  The vector
    ##
    ## @return     [MTransformationMatrix] The rotation matrix from vector.
    ##
    def getRotationMatrixFromVector(self, vector=OpenMaya.MVector()):
        up = OpenMaya.MVector(0, 1, 0)

        xaxis = up ^ vector
        xaxis = xaxis.normalize()

        yaxis = vector ^ xaxis
        yaxis = yaxis.normalize()

        result = OpenMaya.MMatrix()

        result.setElement(0, 0, xaxis.x)
        result.setElement(0, 1, yaxis.x)
        result.setElement(0, 2, vector.x)

        result.setElement(1, 0, xaxis.y)
        result.setElement(1, 1, yaxis.y)
        result.setElement(1, 2, vector.y)

        result.setElement(2, 0, xaxis.z)
        result.setElement(2, 1, yaxis.z)
        result.setElement(2, 2, vector.z)

        return result

    ##
    ## @brief      Actual operative method of the node. Here the mesh will be
    ##             generated based on the value of the varius input attributes.
    ##
    ## @param      self  The object
    ## @param      plug  [MPlug] - The plug
    ## @param      data  [MDataBlock] - The data
    ##
    ## @return     [None] - None
    ##
    def compute(self, plug, data):
        assert(isinstance(data.context(), OpenMaya.MDGContext))
        assert(data.setContext(data.context()) == data)

        inNurbsDataHandle = data.inputValue(RibbonNode.inNurbsCurve)
        inNurbs = inNurbsDataHandle.asNurbsCurve()

        inRibbonCurveSubdivisionsDataHandle = data.inputValue(RibbonNode.inRibbonCurveSubdivisions)
        inCurveSubdivisions = inRibbonCurveSubdivisionsDataHandle.asInt()

        # inRibbonBaseSubdivisionsDataHandle = data.inputValue(RibbonNode.inRibbonBaseSubdivisions)
        # inBaseSubdivisions = inRibbonBaseSubdivisionsDataHandle.asInt()

        inRibbonWidthDataHandle = data.inputValue(RibbonNode.inRibbonWidth)
        ribbonWidth = inRibbonWidthDataHandle.asDouble()

        inProfileCurve = OpenMaya.MRampAttribute(self.thisMObject(), RibbonNode.inRibbonProfileCurve)

        curve = OpenMaya.MFnNurbsCurve(inNurbs)

        vertices = []
        polygonCounts = []
        polygonConnects = []

        alternateVtxPosition = 1

        for subdivision in xrange(0, inCurveSubdivisions + 1):

            # Gets the param based on the subdivision that the final polygon
            # will have
            curveParam = float(subdivision) / float(inCurveSubdivisions) * curve.knotDomain[1]

            # Gets the point on curve that lies in the param location
            curvePoint = curve.getPointAtParam(curveParam)
            curveNormal = curve.normal(curveParam)
            curveNormal.normalize()
            ribbonProfile = inProfileCurve.getValueAtPosition(curveParam)

            # First point coordinate
            x1 = curvePoint + curveNormal * alternateVtxPosition * ribbonProfile * ribbonWidth

            alternateVtxPosition *= -1

            # Second point coordinate
            x2 = curvePoint + curveNormal * alternateVtxPosition * ribbonProfile * ribbonWidth

            curveTangent = curve.tangent(curveParam)
            curveTangent.normalize()

            vertices.append(x1)
            vertices.append(x2)

            if subdivision == 0:
                polygonConnects.append(0)
                polygonConnects.append(1)
            else:
                if len(polygonConnects) > 2:
                    for i in xrange(len(polygonConnects) - 2, len(polygonConnects)):
                        polygonConnects.append(polygonConnects[i])

                polygonConnects.append(max(polygonConnects) + 1)
                polygonConnects.append(max(polygonConnects) + 1)

                polygonCounts.append(4)

        outMeshData = OpenMaya.MFnMeshData()
        outMeshDataObject = outMeshData.create()

        ribbonMesh = OpenMaya.MFnMesh()
        ribbonMesh.create(vertices, polygonCounts, polygonConnects, parent=outMeshDataObject)

        outMeshDataHandle = data.outputValue(RibbonNode.outRibbonMesh)
        outMeshDataHandle.setMObject(outMeshDataObject)


##
## @brief      Initialize all the custom nodes and commands
##
## @param      mobject  The mobject
##
## @return     None
##
def initializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject, "Andrea Rastelli", "1.0", "Any")

    try:
        mplugin.registerNode(kRibbonNodeName, RibbonNode.id, RibbonNode.creator, RibbonNode.initialize)
    except:
        sys.stderr.write('Failed to register node {}\n'.format(kRibbonNodeName))
        raise


##
## @brief      Uninitialize all the nodes and custom commands
##
## @param      mobject  The mobject
##
## @return     None
##
def uninitializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject)

    try:
        mplugin.deregisterNode(RibbonNode.id)
    except:
        sys.stderr.write('Failed to deregister node {}\n'.format(kRibbonNodeName))
        raise
